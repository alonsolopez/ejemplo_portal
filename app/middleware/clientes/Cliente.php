<?php

//incluimos la LibBD
require_once dirname(dirname(__FILE__)) . "/bd/BDLib.php";
require_once dirname(dirname(__FILE__)) . "/bd/CriterioDeBusqueda.php";

class Cliente
{
    //props de Cliente de pdv UTH 
    private $id;
    private $nombre;
    private $apellidoPaterno;
    private $apellidoMaterno;
    private $fechaDeNacimiento;
    private $celular;
    private $telefono;
    private $correo;
    private $password;
    //faltan los datos de domicilio, curo, etc

    //BD
    private $bd;
    //construct
    public function __construct($id, $nom, $apP, $apM, $fecha, $cel, $tel = null, $correo, $pass)
    {
        //inicializa props
        $this->id = $id;
        $this->nombre = $nom;
        $this->apellidoPaterno = $apP;
        $this->apellidoMaterno = $apM;
        $this->fechaDeNacimiento = $fecha;
        $this->celular = $cel;
        $this->telefono = $tel;
        $this->correo = $correo;
        $this->password = $pass;
        //crea conexion
        $this->bd = new BDLib('localhost', 'pdv_uth_bd_v1', 'root', 'root');
    }

    //CRUDSSS

    //login
    public function login($correo, $pass)
    {
        //hacer consulta
        $criterios = [
            "0" => new CriterioDeBusqueda('correo', CriterioDeBusqueda::OP_IGUAL, $correo, true, CriterioDeBusqueda::OP_LOGICO_AND),
            "1" => new CriterioDeBusqueda('password', CriterioDeBusqueda::OP_IGUAL, $pass, true, CriterioDeBusqueda::OP_LOGICO_NONE),
        ];
        //SELECT * FROM clientes WHERE correo = '??' AND password = '??' ..... 
        //consulta
        return $this->bd->consultarConArrayCriterios('clientes', '*', $criterios);
    }

    public function mapearPorId($id)
    {
        $arregloCliente = $this->bd->consultarPorID('clientes', '*', $id);
        // var_dump($arregloCliente);
        // die("dep cliente");
        //inicializa props con datos de arreglo
        $this->id = $arregloCliente[0]['id'];
        $this->nombre = $arregloCliente[0]['nombre'];
        $this->apellidoPaterno = $arregloCliente[0]['apellido_paterno'];
        $this->apellidoMaterno = $arregloCliente[0]['apellido_materno'];
        $this->fechaDeNacimiento = $arregloCliente[0]['fecha_de_nacimiento'];
        $this->celular = $arregloCliente[0]['celular'];
        $this->telefono = $arregloCliente[0]['telefono'];
        $this->correo = $arregloCliente[0]['correo'];
        $this->password = $arregloCliente[0]['password'];
    }

    /* #region  getters y setters */
    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of apellidoPaterno
     */
    public function getApellidoPaterno()
    {
        return $this->apellidoPaterno;
    }

    /**
     * Set the value of apellidoPaterno
     *
     * @return  self
     */
    public function setApellidoPaterno($apellidoPaterno)
    {
        $this->apellidoPaterno = $apellidoPaterno;

        return $this;
    }

    /**
     * Get the value of apellidoMaterno
     */
    public function getApellidoMaterno()
    {
        return $this->apellidoMaterno;
    }

    /**
     * Set the value of apellidoMaterno
     *
     * @return  self
     */
    public function setApellidoMaterno($apellidoMaterno)
    {
        $this->apellidoMaterno = $apellidoMaterno;

        return $this;
    }

    /**
     * Get the value of fechaDeNacimiento
     */
    public function getFechaDeNacimiento()
    {
        return $this->fechaDeNacimiento;
    }

    /**
     * Set the value of fechaDeNacimiento
     *
     * @return  self
     */
    public function setFechaDeNacimiento($fechaDeNacimiento)
    {
        $this->fechaDeNacimiento = $fechaDeNacimiento;

        return $this;
    }

    /**
     * Get the value of celular
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set the value of celular
     *
     * @return  self
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get the value of telefono
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set the value of telefono
     *
     * @return  self
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get the value of correo
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set the value of correo
     *
     * @return  self
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get the value of password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }
    /* #endregion */
}