<?php

//incluimos la LibBD
require_once dirname(dirname(__FILE__)) . "/bd/BDLib.php";
require_once dirname(dirname(__FILE__)) . "/bd/CriterioDeBusqueda.php";

class OpinionReview
{
    //props de Cliente de pdv UTH 
    private $id;
    private $cliente;
    private $calificacion;
    private $opinion;
    private $fechaHrs;


    //BD
    private $bd;
    //construct
    public function __construct($id, $cliente, $calificacion, $opinion)
    {
        //inicializa props
        $this->id = $id;
        $this->cliente = $cliente;
        $this->calificacion = $calificacion;
        $this->opinion = $opinion;

        //crea conexion con datos de configbd.ini
        $this->bd = new BDLib();
    }


    public function registraOpinionReview($cliente, $calificacion, $opinion)
    {

        //se definen los campos a insertar
        $campos = "cliente_id, calificacion, opinion, fecha_hrs";
        //tomamos el ID del array de cliente en la sesión
        $clienteId = $cliente['id'];
        //ahora los VALUES
        $valores = "$clienteId, $calificacion, '$opinion', NOW()";
        //se retuliza el insert de BDLIB
        return $this->bd->insertar('opiniones_reviews', $campos, $valores);
    }

    public function consultar($criterios)
    {
        return $this->bd->consultarConArrayCriterios('opiniones_reviews', '*', $criterios);
    }

    public function consultarTodos()
    {
        return $this->bd->consultarTodos('opiniones_reviews');
    }
}