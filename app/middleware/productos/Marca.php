<?php
//incluimos la LibBD
require_once dirname(dirname(__FILE__)) . "/bd/BDLib.php";
require_once dirname(dirname(__FILE__)) . "/bd/CriterioDeBusqueda.php";
class Marca
{
    private $id;
    private $nombre;
    private $descripcion;
    private $logoURL;
    private $bd;
    public function insertar()
    {
    }

    //constructor
    public function __construct()
    {
        //crea conexion
        $this->bd = new BDLib();
    }

    //métodos
    public function consultarTodas()
    {
        return $this->bd->consultarTodos('marcas');
    }


    /* #region  getters y setters */
    //getters Setters
    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of descripcion
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @return  self
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }
    /* #endregion */

    /**
     * Get the value of logoURL
     */
    public function getLogoURL()
    {
        return $this->logoURL;
    }

    /**
     * Set the value of logoURL
     *
     * @return  self
     */
    public function setLogoURL($logoURL)
    {
        $this->logoURL = $logoURL;

        return $this;
    }
}