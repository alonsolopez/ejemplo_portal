<?php
//incluimos la LibBD
require_once dirname(dirname(__FILE__)) . "/bd/BDLib.php";
require_once dirname(dirname(__FILE__)) . "/bd/CriterioDeBusqueda.php";
class Categoria
{
    //props
    private $id;
    private $nombre;
    private $descripcion;

    private $bd;

    //constructor
    public function __construct()
    {

        //crea conexion
        $this->bd = new BDLib();
    }

    //métodos
    /**
     * Me regresa un ARREGLO con los registros de las Categorias en la tabla 'categorias'
     *
     * @return array  Regresa NUll cuando no hay Categorias
     */
    public function consultarTodos(): array
    {
        return $this->bd->consultarTodos('categorias');
    }

    /**
     * Genera UNA cadena con los options que pondremos en los formularios
     * dentro de una etiqueta SELECT (como comboBox).
     *
     * @return string
     */
    public function consultarOptionsParaSelect(): string
    {
        //definimos cadena de OPctions resultante
        $options = "";
        //rellenamos los options con las marcas
        foreach ($this->bd->consultarTodos('categorias') as $key => $value) {
            //generamos los options

            $options .= "<option value=" . $value["id"] . " title=" . $value["descripcion"] . ">" . $value["nombre"] . "</option><br>";
        }
        //regresamos la CADENA de las options
        return $options;
    }

    /* #region  getters y setters */
    //getters Setters
    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of descripcion
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @return  self
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }
    /* #endregion */
}