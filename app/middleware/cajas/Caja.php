<?php
require_once dirname(dirname(__FILE__)) . "/bd/BDLib.php";
require_once dirname(dirname(__FILE__)) . "/bd/CriterioDeBusqueda.php";
// require_once "../bd/BDLib.php";

class Caja
{
    //instancia de BD
    private $bd;
    //props de la CLASE!!!
    private $id;
    private $nombre;
    private $macAddress;
    private $descripcion;
    //getters setters
    //setters---- establecer un valor a la prop private
    /* #region  setters  */
    public function setId($value)
    {
        $this->id = $value;
    }
    public function setNombre($value)
    {
        $this->nombre = $value;
    }
    public function setMacAddress($value)
    {
        $this->macAddress = $value;
    }
    public function setDescripcion($value)
    {
        $this->descripcion = $value;
    }
    /* #endregion */
    /* #region  getters */
    //getters--- obtener el valor de la prop para imprimir
    public function getId()
    {
        return $this->id;
    }
    public function getNombre()
    {
        return $this->nombre;
    }
    public function getMacAddress()
    {
        return $this->macAddress;
    }
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    /* #endregion */

    //constructor (es)           '',    '',    ''
    public function __construct($nombre, $mac, $descrip)
    {

        //inicializar pars
        $this->nombre = $nombre;
        $this->macAddress = $mac;
        $this->descripcion = $descrip;

        //inicializa BD
        // $this->bd = new BDLib();
        $this->bd = new BDLib("localhost", "pdv_uth_bd_v1", "root", "root",);
    }
    //métodos
    //---- CRUD
    public function insertar($nom, $mac, $descrip)
    {
        $datosAInsertar = [
            'nombre'         => $nom,
            'mac_address'    => $mac,
            'descripcion'    => $descrip,
        ];
        $campos = '';
        $valores = '';
        foreach ($datosAInsertar as $idx => $dato) {
            # code...
            print $idx . "=" . $dato;
            $campos .= $idx . ',';
            $valores .= "'$dato',";
        }
        // $campos = array_keys($datosAInsertar)->implode(',');
        $campos = rtrim($campos, ',');
        $valores = rtrim($valores, ', ');
        if ($this->bd->insertar('cajas', $campos, $valores)) {

            return true;
        } else {
            //devolvemos false en caso de haber error
            return false;
        }
    }

    public function actualizar($nom, $mac, $descrip, $id)
    {
        $datosAModif = [
            'nombre'         => $nom,
            'mac_address'    => $mac,
            'descripcion'    => $descrip,
        ];
        return $this->bd->actualizar('cajas', $datosAModif, $id);
    }

    public function eliminar($id)
    {
        return $this->bd->eliminar('cajas', $id);
    }

    public function modificar($nom, $mac, $descrip, $id)
    {
        $camposVals = [
            'nombre' => $nom,
            'mac_address' => $mac,
            'descripcion' => $descrip,
        ];
        return $this->bd->actualizar('cajas', $camposVals, $id);
    }

    public function consultarTodos()
    {
        //                    SELECT * FROM cajas WHERE 1
        return $this->bd->consultar('cajas', '*', '1');
    }



    public function consultaEspecifica($arrayCriterios)
    {
        return $this->bd->consultarConArrayWhere('cajas', '*', $arrayCriterios);
    }

    public function consultaEspecificaArray($arrayCriteriosBusqueda)
    {
        return $this->bd->consultarConArrayCriterios('cajas', '*', $arrayCriteriosBusqueda);
    }

    ///alta, baja, modificaciones y CONSULTAS!!!
    //--- funcionalidad especifica
    ///vender... tickets... 
}