<?php

//incluimos la LibBD
require_once dirname(dirname(__FILE__)) . "/bd/BDLib.php";
require_once dirname(dirname(__FILE__)) . "/bd/CriterioDeBusqueda.php";

class Usuario
{
    //props de Usuario de pdv UTH 
    private $id;
    private $nombre;
    private $apellidoPaterno;
    private $apellidoMaterno;
    private $fechaDeNacimiento;
    private $celular;
    private $telefono;
    private $correo;
    private $password;
    //faltan los datos de domicilio, curo, etc

    //BD
    private $bd;
    //construct
    public function __construct($id, $nom, $apP, $apM, $fecha, $cel, $tel = null, $correo, $pass)
    {
        //inicializa props
        $this->id = $id;
        $this->nombre = $nom;
        $this->apellidoPaterno = $apP;
        $this->apellidoMaterno = $apM;
        $this->fechaDeNacimiento = $fecha;
        $this->celular = $cel;
        $this->telefono = $tel;
        $this->correo = $correo;
        $this->password = $pass;
        //crea conexion
        $this->bd = new BDLib();
    }

    //CRUDSSS

    //login
    public function login($correo, $pass)
    {
        //hacer consulta
        $criterios = [
            "0" => new CriterioDeBusqueda('correo', CriterioDeBusqueda::OP_IGUAL, $correo, true, CriterioDeBusqueda::OP_LOGICO_AND),
            "1" => new CriterioDeBusqueda('pwd', CriterioDeBusqueda::OP_IGUAL, $pass, true, CriterioDeBusqueda::OP_LOGICO_NONE),
        ];
        //SELECT * FROM usuarios WHERE correo = '??' AND pwd = '??' ..... 
        //consulta
        return $this->bd->consultarConArrayCriterios('usuarios', '*', $criterios);
    }
}