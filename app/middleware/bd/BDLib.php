<?php



class BDLib
{
    //vars de conexion
    private $protocolo; //oracle
    private $host;
    private $us;
    private $pwd;
    private $port; //3306
    private $dbname;
    //var de manejo de acciones de BD
    private $dsn;
    private $dbh;
    private $options;
    //mensaje de error
    public static string $msgError = '';


    //constructor
    /**
     * Constructor de Objeto BDLib, libreria de BD on MYSQL
     * Permite generar una conexion con los datos establecidos 
     * empleando PDO
     *
     * @param string $host  direccion IP o DOMINIO del servidor de BASE DE DATOS
     * @param string $dbname Nombre de la BASE DE DATOS que vamos a emplear
     * @param string $us
     * @param string $pwd
     * @param integer $port Parametro OPCIONAL, vale 8889 por default, por que es el puerto de mi localhost
     */
    public function __constructor($host, $dbname, $us, $pwd, $port = 8889)
    {
        $this->host = $host;
        $this->dbname = $dbname;
        $this->us = $us;
        $this->pwd = $pwd;
        $this->port = $port;
        //datos por default
        if (func_num_args() == 0) {
            $this->host = 'localhost';
            $this->dbname = 'pdv_uth_bd_v1';
            $this->us = 'root';
            $this->pwd = 'root';
        }
        //data spource
        $this->dsn = "$this->protocolo:host=$this->host;port=$this->port;dbname=$this->dbname";
        //optiones para el ERR MODE
        $this->options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];
    }

    public function __construct()
    {
        //se leen los pars de arch ini    // parse_ini_file('../../config/configbd.ini');
        $config = parse_ini_file(dirname(dirname(dirname(__FILE__))) . "/config/configbd.ini");

        // foreach ($config as $key => $value) {
        //     echo "<br>$key => $value";
        // }

        //se obtienen los datos de arch de config INI
        $this->protocolo = $config['driver'];
        $this->host = $config['host'];
        $this->dbname = $config['dbname'];
        $this->us = $config['us'];
        $this->pwd = $config['pwd'];
        $this->port = $config['port'];
        //data spource
        $this->dsn = "$this->protocolo:host=$this->host;port=$this->port;dbname=$this->dbname";
        //optiones para el ERR MODE
        $this->options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];
    }


    //métodos
    /**
     * abrir la Conexión instancada en el CONSTRUCTOR
     *
     * @return true si se abre la conexión, false si hay error, y checar MSG de ERROR
     */
    public function abrirConexion()
    {
        try {
            //se abre conexion CREANDO INSTANCIA
            $this->dbh = new PDO($this->dsn, $this->us, $this->pwd, $this->options);
            return true;
        } catch (PDOException $ex) {
            print "Error al conectar " . $ex->getMessage();
            die();
        }
        return false;
    }

    public function cerrarConexion()
    {
        $this->dbh = null;
    }
    //DDL
    //DML
    public function actualizar($tabla, $camposValores, $idRegistroAModificar)
    {

        // $arregloCamposValores = [
        //     'nombre' => 'Caja Modificada',
        //     'mac_address' => '009900AABBCC',
        // ];

        // "UPDATE cajas SET `nombre`= :nombre, `mac_address`= :mac_address WHERE id = $idRegistroAModificar "

        $camposVals = '';
        $resultado = false;
        try {
            //conexion PDO
            if ($this->abrirConexion()) {
                //query
                $command = "UPDATE `$tabla` SET  ";
                //se obtienen los pares `campo`=valor
                foreach ($camposValores as $idx => $valor) {
                    //concatena para el QUERY
                    $camposVals .= " `$idx`= :$idx, ";
                    //cambia las llaves
                    $camposValores[":$idx"] = $camposValores["$idx"];
                    //borrar el idx anterior
                    unset($camposValores["$idx"]);
                }
                $camposVals = rtrim($camposVals, ' ,');
                $command .= $camposVals . " WHERE id = $idRegistroAModificar";

                // echo "ID a modif " . $idRegistroAModificar;
                // var_dump($command, $camposValores);
                //ejecutar
                $stmt = $this->dbh->prepare($command);
                $res = $stmt->execute($camposValores);
                print('Resultado:' . $res . ' Registros MODIFICADOS : ' . $stmt->rowCount());
                // die('depurando');
                //responder
                if ($res  && $stmt->rowCount() > 0) {
                    echo "Si entro para cambiar el resultado = true";
                    $resultado = true;
                }
            }
        } catch (PDOException $ex) {
            //veruficar error
            print("Error en el BORRAR registro en -$tabla-  " . $ex->getMessage());
        } finally {
            $this->cerrarConexion();
        }
        //devolvemos false en caso de haber error
        return $resultado;
    }

    public function eliminar($tabla, $idRegistroABorrar)
    {
        $resultado = false;
        try {
            //conexion PDO
            if ($this->abrirConexion()) {
                //query
                $command = "DELETE FROM `$tabla` WHERE id = $idRegistroABorrar";
                var_dump($command);
                //ejecutar
                $res = $this->dbh->query($command);
                print $res->rowCount();
                // die();
                //responder
                if ($res == true && $res->rowCount()) {
                    $resultado = true;
                }
            }
        } catch (PDOException $ex) {
            //veruficar error
            print("Error en el BORRAR registro en -$tabla-  " . $ex->getMessage());
            BDLib::$msgError = "Error en el BORRAR registro en -$tabla-  " . $ex->getMessage();
        } finally {
            $this->cerrarConexion();
        }
        //devolvemos false en caso de haber error
        return $resultado;
    }
    public function insertarDevolviendoID($tabla, $campos, $valores)
    {
        $resultado = 0;
        try {
            //conexion PDO
            if ($this->abrirConexion()) {
                //query                         nombre, mac_address, descripcion  ('nom', '99DFEDA', 'descripc....')
                $command = "INSERT INTO `$tabla` ($campos) VALUES ($valores)";
                // var_dump($command);
                // die('Depurando INSERT');
                //ejecutar
                $res = $this->dbh->query($command);
                print('registros insertados=' . $res->rowCount());
                //responder
                if ($res) {
                    //ejecutamos el lastINsertId
                    $resultado = $this->dbh->lastInsertId();
                    var_dump($resultado);
                    // die('Last insert id...');
                }
            } else BDLib::$msgError = "No se puede INSERTAR, Problema con la conexión";
        } catch (PDOException $ex) {
            //veruficar error
            print("Error en el INSERTAR registro en -$tabla-  " . $ex->getMessage());
            BDLib::$msgError = "Error en el INSERTAR registro en -$tabla-  " . $ex->getMessage();
        } finally {
            $this->cerrarConexion();
        }
        //devolvemos false en caso de haber error
        return $resultado;
    }

    public function insertarDevolviendoIDArrayDeEntrada($tabla, $camposValores)
    {
        $camposVals = '';
        $resultado = false;
        try {
            //conexion PDO
            if ($this->abrirConexion()) {
                //necesitamos tener separado, campos y valores
                $campos = '';
                $valores = '';
                //empezamos la sentencia INSERT
                $command = "INSERT INTO `$tabla`  ";
                //se obtienen los pares `campo`=valor y se pasan a `campo:`=valor
                foreach ($camposValores as $idx => $valor) {
                    //concatena para el INSERT
                    $campos .= " `$idx`, "; //los campos (`c1`, `c2`,... `etc`)
                    $valores .= " :$idx, "; //los  VALUES (:v1, :v2,  ... :etc)
                    //cambia las llaves a [:idx] = 'valor'
                    $camposValores[":$idx"] = $camposValores["$idx"];
                    //borrar el idx anterior que no tiene ':'
                    unset($camposValores["$idx"]);
                }
                //quitamos la ultima ',' de las cadenas contatenadas
                $campos = rtrim($campos, ' ,');
                $valores = rtrim($valores, ' ,');
                //concatenamos el  query
                $command .= " ($campos) VALUES($valores) ";

                // echo "ID a modif " . $idRegistroAModificar;
                // var_dump($command, $camposValores);
                //ejecutar
                $stmt = $this->dbh->prepare($command);
                $res = $stmt->execute($camposValores);
                //obtenemos lastInsertID
                $ultimoIDGenerado = $stmt->lastInsertId();
                print('Resultado:' . $res . ' Registro INSERTADO : ' . $stmt->rowCount() . ', con ID=' . $ultimoIDGenerado);
                die('depurando');
                //responder
                if ($res  && $stmt->rowCount() > 0) {
                    echo "Si entro para cambiar el resultado = true";
                    $resultado = true;
                }
            }
        } catch (PDOException $ex) {
            //veruficar error
            print("Error en el BORRAR registro en -$tabla-  " . $ex->getMessage());
        } finally {
            $this->cerrarConexion();
        }
        //devolvemos false en caso de haber error
        return $resultado;
    }

    //CONSULTAS
    public function consultar($tabla, $campos, $where)
    {
        try {
            $command = "SELECT $campos FROM $tabla WHERE $where";
            if ($this->abrirConexion()) {
                echo '<br>';
                // $res = $this->dbh->query($command)->fetchAll(PDO::FETCH_CLASS, 'Caja');
                $res = $this->dbh->query($command); //->fetch(PDO::FETCH_BOTH);
                foreach ($res as $row) {
                    print_r($row);
                    echo '<br>';
                }
            }
        } catch (PDOException $ex) {
            # code...
            print("Error en la consulta" . $ex->getMessage());
        }
    }

    public function consultarTodos($tabla)
    {
        try {
            $command = "SELECT * FROM $tabla WHERE 1";
            if ($this->abrirConexion()) {
                // $res = $this->dbh->query($command)->fetchAll(PDO::FETCH_CLASS, 'Caja');
                $stmt = $this->dbh->prepare($command); //->fetch(PDO::FETCH_BOTH);
                $resEjecucion = $stmt->execute();
                //obtenemos el arreglo con RESULTADOS de consulta
                $res = $stmt->fetchAll();
                //verificamos si tiene al menos un resultado
                //  $resCount=count($res);
                // print('RES===' . $resEjecucion . " count===" . $resCount . '<BR>');

                //devolvemos todo el ARREGLO resultante
                return count($res) > 0 ? $res : null;
            }
        } catch (PDOException $ex) {
            # code...
            print("Error en la consulta" . $ex->getMessage());
        }
    }

    public function consultarPorID($tabla, $campos, $id)
    {
        try {
            $command = "SELECT $campos FROM $tabla WHERE id=$id";
            if ($this->abrirConexion()) {
                // $res = $this->dbh->query($command)->fetchAll(PDO::FETCH_CLASS, 'Caja');
                $stmt = $this->dbh->prepare($command); //->fetch(PDO::FETCH_BOTH);
                $resEjecucion = $stmt->execute();
                //obtenemos el arreglo con RESULTADOS de consulta
                $res = $stmt->fetchAll();
                //verificamos si tiene al menos un resultado
                //  $resCount=count($res);
                // print('RES===' . $resEjecucion . " count===" . $resCount . '<BR>');

                //devolvemos todo el ARREGLO resultante
                return count($res) > 0 ? $res : null;
            }
        } catch (PDOException $ex) {
            # code...
            print("Error en la consulta" . $ex->getMessage());
        }
    }

    public function consultarConArrayWhere($tabla, $campos, $where)
    {
        //var de resultado de consulta, valor por DEFAULT null
        $res = null;
        try {
            $command = "SELECT $campos FROM $tabla WHERE ";
            //se agregan los WHERE y se convierten los keys a :keys
            foreach ($where as $key => $value) {
                $command .= "`$key` = :$key AND ";
                //cambiar el KEY por :KEY...
                $where[":$key"] = $where[$key];
                unset($where[$key]);
            }
            //se elimina el último AND
            $command = rtrim($command, ' AND');
            //imprimir el query, para verificar
            echo $command . '<BR>';
            // print_r($where);
            // echo '<BR>';
            //se abre conexión y se procede con la consulta
            if ($this->abrirConexion()) {
                // $res = $this->dbh->query($command)->fetchAll(PDO::FETCH_CLASS, 'Caja');
                $stmt = $this->dbh->prepare($command); //->fetch(PDO::FETCH_BOTH);
                $resEjecucion = $stmt->execute($where);
                //obtenemos el arreglo con RESULTADOS de consulta
                $res = $stmt->fetchAll();
                //verificamos si tiene al menos un resultado
                //  $resCount=count($res);
                // print('RES===' . $resEjecucion . " count===" . $resCount . '<BR>');

                //devolvemos todo el ARREGLO resultante
                return count($res) > 0 ? $res : null;
            }
        } catch (PDOException $ex) {
            # code...
            print("Error en la consulta" . $ex->getMessage());
        } finally {
            $this->cerrarConexion();
        }
        //regresamos o NULL, o los resultados de la consulta
    }



    public function consultarConArrayCriterios($tabla, $campos, $criterios)
    {
        //var de resultado de consulta, valor por DEFAULT null
        $res = null;
        try {
            $command = "SELECT $campos FROM $tabla WHERE ";
            //se agregan los criterios y se convierten los keys a :keys
            foreach ($criterios as $key => $value) {
                $command .= $value->get() . " ";
            }
            //imprimir el query, para verificar
            echo $command . '<BR>';
            print_r($criterios);
            echo '<BR>';
            //se abre conexión y se procede con la consulta
            if ($this->abrirConexion()) {
                // $res = $this->dbh->query($command)->fetchAll(PDO::FETCH_CLASS, 'Caja');
                $stmt = $this->dbh->prepare($command); //->fetch(PDO::FETCH_BOTH);
                $resEjecucion = $stmt->execute(); //->fetch(PDO::FETCH_BOTH);
                //obtenemos el arreglo con RESULTADOS de consulta
                $res = $stmt->fetchAll();
                //verificamos si tiene al menos un resultado
                $resCount = count($res);
                // print_r($res);
                // print(" count===" . $resCount . '<BR>');
                //devolvemos todo el ARREGLO resultante o NULL
                $res = $resCount > 0 ? $res : null;
            }
        } catch (PDOException $ex) {
            # code...
            print("Error en la consulta" . $ex->getMessage());
        } finally {
            $this->cerrarConexion();
        }
        //regresamos o NULL, o los resultados de la consulta
        return $res;
    }
}