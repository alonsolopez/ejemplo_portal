<?php
session_start();
//required CLIENTE
require_once "../middleware/usuarios/Usuario.php";
//si se ingreso datos en el FORM
$mail = "";
$pass = "";
if (isset($_POST['btnLogin'])) {
    //giardamos los datos
    if (isset($_POST['email']))
        $mail = $_POST['email'];
    if (isset($_POST['pass']))
        $pass = $_POST['pass'];
    //creamos la instancia
    $usuario = new Usuario('', '', '', '', '', '', '', '', '');
    $logeado = 'nada';
    //ejecutamos el login
    if (($logeado = $usuario->login($mail, $pass)) && $logeado != null) {
        //var_dump($logeado);
        // die();
        //crear las vars de sesión
        $_SESSION['usuarioLogeado'] = $logeado[0];
        var_dump($_SESSION['usuarioLogeado']);
        //die();
        //abrimos HOME
        header("location:index.php");
    } else {
        echo "Error al ingresar, verifique sus datos e inténtelo de nuevo";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Ingresa al PORTAL EMPRESARIAL PDV UTH</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="./imgs/icons/favicon.ico" />
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!-- CSS -->
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="./css/util.css">
    <link rel="stylesheet" type="text/css" href="./css/main.css">
    <!--===============================================================================================-->
</head>

<body>

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-pic js-tilt" data-tilt>
                    <img src="./imgs/LOGO-HOR uth.png" alt="UTH IMG">
                </div>

                <form class="login100-form validate-form" method="POST">
                    <span class="login100-form-title">
                        Ingreso de EMPLEADOS
                    </span>

                    <div class="wrap-input100 validate-input" data-validate="Un Correo válido es requerido: ex@abc.xyz">
                        <input class="input100" type="text" name="email" placeholder="Tu Correo Registrado">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="La contraseña es requerida">
                        <input class="input100" type="password" name="pass" placeholder="Tu Contraseña">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </span>
                    </div>

                    <div class="container-login100-form-btn">
                        <button name="btnLogin" class="login100-form-btn">
                            Login
                        </button>
                    </div>

                    <div class="text-center p-t-12">
                        <span class="txt1">
                            Olvidaste
                        </span>
                        <a class="txt2" href="#">
                            ¿Correo / Contraseña?
                        </a>
                    </div>

                    <div class="text-center p-t-136">
                        <a class="txt2" href="#">
                            Crea tu Cuenta
                            <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>




    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/tilt/tilt.jquery.min.js"></script>
    <script>
    $('.js-tilt').tilt({
        scale: 1.1
    })
    </script>
    <!--===============================================================================================-->
    <script src="js/main.js"></script>

</body>

</html>