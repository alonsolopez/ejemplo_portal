<header class="">
    <div class="row header">
        <div class="col-sm-3">
            <div class="row">
                <div class="col" id="logo">
                    <img src="../../public/images/home/logoApp.png" alt="Logo">
                </div>
                <div class="col">
                    <h1>Portal Empresarial</h1>
                </div>
            </div>
        </div>
        <div class="col"></div>
        <div class="col-sm-8 cuenta">
            <div class="row">
                <h4>Usuario:</h4>
                <p>Nombre </p><br>
            </div>
            <div class="row">
                <h4>Tipo:</h4>
                <p><b>ADMIN</b></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">

            <nav class="navbar navbar-expand-lg navbar-light bg-naranja">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
                    aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarToggler">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item active">
                            <a class="nav-link" href="http://localhost:8888/php/ejemplo/app/views/">Home <span
                                    class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Almacenista</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Productos
                            </a>
                            <div class="dropdown-menu bg-azul" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item"
                                    href="http://localhost:8888/php/ejemplo/app/views/pages/productos/home_crud.php">Home
                                    Crud</a>
                                <a class="dropdown-item"
                                    href="http://localhost:8888/php/ejemplo/app/views/pages/productos/crear_producto.php">Crear
                                    Nuevo</a>

                                <a class="dropdown-item" href="#">Comprar</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                        </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
                    </form>
                </div>
            </nav>

        </div>
    </div>
</header>