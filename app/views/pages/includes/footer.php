<footer class="footer bg-dark center">
    <h4>PDV UTH v1 DR®2020</h4>
    <hr>
    <h5>Mapa del Sitio</h5>
    <hr>
    <div class="row">
        <div class="col-sm-3" id="menu-left">
            <h6>Sección 1</h6>
            <ul>
                <li><a href="#1">opcion1</a></li>
                <li><a href="#2">opcion2</a></li>
                <li><a href="#3">opcion3</a></li>
                <li><a href="#4">opcion4</a></li>
                <li><a href="#5">opcion5</a></li>
                <li><a href="#6">opcion6</a></li>
            </ul>
        </div>
        <div class="col-sm-3" id="menu-middle">
            <h6>Sección 2</h6>
            <ul>
                <li><a href="#1">opcion1</a></li>
                <li><a href="#2">opcion2</a></li>
                <li><a href="#3">opcion3</a></li>
                <li><a href="#4">opcion4</a></li>
                <li><a href="#5">opcion5</a></li>
                <li><a href="#6">opcion6</a></li>
            </ul>
        </div>
        <div class="col-sm-3" id="menu-right">
            <h6>Sección 3</h6>
            <ul>
                <li><a href="#1">opcion1</a></li>
                <li><a href="#2">opcion2</a></li>
                <li><a href="#3">opcion3</a></li>
                <li><a href="#4">opcion4</a></li>
                <li><a href="#5">opcion5</a></li>
                <li><a href="#6">opcion6</a></li>
            </ul>
        </div>
</footer>