<?php

class DataGrid
{

    private $headers;
    private $data;
    private $acciones;

    public function show($headers, $data, $acciones)
    {
?>
<table class="table table-hover">
    <thead>
        <tr>
            <?php
                    //headers----
                    foreach ($headers as $header) { ?>
            <th scope="col">$header</th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <?php
                foreach ($data as $index => $value) {
                ?>
        <tr>
            <th scope="row"><?php echo $index; ?></th>
            <?php
                        foreach ($value as $idx => $dato) {
                        ?>
            <td><?php echo $dato; ?></td>
            <?php
                        } //foreach dato
                        ?>
        </tr>
        <?php
                } //for each renglon

                ?>
    </tbody>
</table>
<?php
    } //fin de método
} //class DataGrid
?>