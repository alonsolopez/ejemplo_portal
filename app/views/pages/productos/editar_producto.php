<?php
//verificar que Admin o Almacenista haya hecho sesión
session_start();
if (isset($_SESSION['usuarioLogeado'])) {
    //si hay sesión se obtienen los datos del tipo de Usuario
    $tipoUs = $_SESSION['usuarioLogeado']['tipo_usuario'];
} else { //si no hay sesion, que se vaya a LOGIN
    header('location:../login.php');
}

//se carga codigo de Producto para CRUD
require_once '../../../middleware/productos/Producto.php';
//se crea instancia de Producto VACIO
$producto = new Producto('', '', '', '', '', '', '', '');


//codigo para modifica
if (isset($_POST['btnProducto'])) {
    //obtenemos los datos
    //validando cada input
    if (isset($_POST['txtId']))           $txtID = $_POST['txtId'];
    if (isset($_POST['txtNombre']))       $txtNombre = $_POST['txtNombre'];
    if (isset($_POST['txtDescripcion']))  $txtDescripcion = $_POST['txtDescripcion'];
    if (isset($_POST['txtPrecio']))       $txtPrecio = $_POST['txtPrecio'];
    if (isset($_POST['txtCodBarras']))    $txtCodBarras = $_POST['txtCodBarras'];

    //crear arreglo con los datos para UPDATE
    $datosActuales = [
        "nombre" => $txtNombre,
        "descripcion" => $txtDescripcion,
        "precio" => $txtPrecio,
        "codigo_barras" => $txtCodBarras,
    ];


    //ejecutamos el metodo update del obj producto
    $res = $producto->update($datosActuales, $txtID);
    // echo "res=" . $res;
    // die('depurando despues de update');
    //verificar que si se haya actualizado
    if ($res != false) {
        echo "<script>alert('Si se actualizó el registro. RES=" . $res . ");</script>";
        header('location:home_crud.php');
    } else echo "Error, no se actualizaron productos";
}


//verificar si llegó producto_id por POST
if (isset($_GET['producto_id']) && $_GET['producto_id'] > 0) {
    // var_dump($producto);

    $prod = $producto->consultaPorID($_GET['producto_id'])[0];
    //mostrar todos los datos

} else {
    header('location:./home_crud.php');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portal empresarial - PDV UTH v1</title>
    <!-- CSS -->
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
    <link href="../../css/main.css" rel="stylesheet">
    <link href="../../css/responsive.css" rel="stylesheet">
    <!-- CSS -->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
</head>

<body>
    <div class="container">
        <?php include "../includes/header.php"; ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-2" id="menu-left">
                    <h4>Menu left</h4>
                    <ul>
                        <li><a href="#1">opcion1</a></li>
                        <li><a href="#2">opcion2</a></li>
                        <li><a href="#3">opcion3</a></li>
                        <li><a href="#4">opcion4</a></li>
                        <li><a href="#5">opcion5</a></li>
                        <li><a href="#6">opcion6</a></li>
                    </ul>
                </div>
                <div class="col-sm-9">
                    <div class="row">

                        <div class="col-sm-6">
                            <form action="" method="POST">
                                <!-- datos -->
                                <div class="form-group">
                                    <label for="txtId">ID</label>
                                    <input type="text" class="form-control" id="txtId" name="txtId"
                                        aria-describedby="idHelp" value="<?php echo $prod['id']; ?>">
                                    <small id="idHelp" class="form-text text-muted">El ID asignado por el motro de
                                        BD.</small>
                                </div>
                                <div class="form-group">
                                    <label for="txtNombre">Nombre</label>
                                    <input type="text" class="form-control" name="txtNombre" id="txtNombre"
                                        aria-describedby="nombreHelp" value="<?php echo $prod['nombre']; ?>">
                                    <small id="nombreHelp" class="form-text text-muted">El nombre del producto
                                        registrado.</small>
                                </div>
                                <div class="form-group">
                                    <label for="txtDescripcion">Descripcion</label>
                                    <textarea class="form-control" id="txtDescripcion" name="txtDescripcion"
                                        aria-describedby="codBarrasHElp"><?php echo $prod['descripcion']; ?></textarea>
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with
                                        anyone else.</small>
                                </div>
                                <div class="form-group">
                                    <label for="txtPrecio">Precio</label>
                                    <input type="text" class="form-control" id="txtPrecio" name="txtPrecio"
                                        aria-describedby="precioHelp" value="<?php echo $prod['precio']; ?>">
                                    <small id="precioHelp" class="form-text text-muted">Precio del producto registrado
                                        .</small>
                                </div>
                                <div class="form-group">
                                    <label for="txtCategoria">Código de Barras</label>
                                    <input type="text" class="form-control" name="txtCodBarras" id="txtCodBarras"
                                        aria-describedby="emailHelp" value="<?php echo $prod['codigo_barras']; ?>">
                                    <small id="codBarrasHElp" class="form-text text-muted">El código de Barras de
                                        producto.</small>
                                </div>
                                <div class="form-group">
                                    <label for="txtMarca">Marca</label>
                                    <input type="text" class="form-control" name="txtMarca" id="txtMarca"
                                        aria-describedby="marcaHelp"
                                        value="<?php echo $producto->getNombreDeMarca($prod['marca_id']); ?>">
                                    <small id="marcaHelp" class="form-text text-muted">Marca de
                                        producto.</small>
                                </div>

                                <div class="form-group">
                                    <label for="txtCategoria">Categoria</label>
                                    <input type="text" class="form-control" name="txtCategoria" id="txtCategoria"
                                        aria-describedby="categoriaHElp"
                                        value="<?php echo $producto->getNombreDeCategoria($prod['categoria_id']); ?>">
                                    <small id="categoriaHElp" class="form-text text-muted">La categoria de
                                        producto.</small>
                                </div>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1"
                                        value="<?php echo $prod['es_perecedero'] ?>">
                                    <label class="form-check-label" for="exampleCheck1">¿Perecedero?</label>
                                </div>
                        </div>
                        <div class="col-sm-6">
                            <!-- imagen -->
                            <div class="card" style="width: 18rem;">
                                <label for="imagen">Imagen</label>
                                <img src="../../../../public/<?php echo $prod['imagen_producto']; ?>"
                                    class="card-img-top" alt="...">
                                <div class="card-body">
                                    <p class="card-text">La Imagen de producto..</p>
                                </div>
                            </div>



                        </div>
                        <!-- button -->
                        <button type="submit" class="btn btn-warning" name="btnProducto">Actualizar</button>
                        </form>
                    </div>
                    <main class="row">
                        <div class="col">
                            <h2>Productos</h2>
                            <hr>
                            <table class="table table-hover table-sm bg-azul rounded-top" border="1">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Descrpcion</th>
                                        <th>Precio</th>
                                        <th>Cod.Barras</th>
                                        <th>Imágenes</th>
                                        <th>Marca</th>
                                        <th>Categoria</th>
                                        <th colspan="3">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td><?php echo $prod['id']; ?></td>
                                        <td><?php echo $prod['nombre']; ?></td>
                                        <td><?php echo $prod['descripcion']; ?></td>
                                        <td><?php echo $prod['precio']; ?></td>
                                        <td><?php echo $prod['codigo_barras']; ?></td>
                                        <td><img width="25%"
                                                src="../../../../public/<?php echo $prod['imagen_producto']; ?>"
                                                alt="<?php echo $prod['imagen_producto']; ?>"></td>
                                        <td><?php echo $producto->getNombreDeMarca($prod['marca_id']); ?></td>
                                        <td><?php echo $producto->getNombreDeCategoria($prod['categoria_id']); ?></td>

                                        <!-- acciones de registro para CRUD -->
                                        <!-- MOSTRAR - CONSULTA de todos los datos de producto -->
                                        <td><a href="mostrar_producto.php?producto_id=<?php echo $prod['id']; ?>"
                                                class="btn btn-primary">Mostrar</a></td>

                                        <td><a href="editar_producto.php?producto_id=<?php echo $prod['id']; ?>"
                                                class="btn btn-warning">Editar</a>

                                        </td>
                                        <td>
                                            <!-- <form action="eliminar_producto.php?producto_id=<?php echo $prod['id']; ?>"
                                                method="GET">
                                                <button
                                                    onclick="return confirm('¿Esta seguro de que quiere eliminar el producto ID #<?php echo $value['id']; ?>?');"
                                                    type="submit" class="btn btn-danger">Eliminar</button>
                                            </form> -->
                                            <a class="btn btn-danger"
                                                onclick="return confirm('¿Esta seguro de que quiere eliminar el producto ID #<?php echo $value['id']; ?>?');"
                                                href="eliminar_producto.php?producto_id=<?php echo $value['id']; ?>">Borrar</a>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </main>
                </div>
            </div>
        </div>
        <?php include '../includes/footer.php'; ?>
    </div>
    <!-- JS  -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>

    <!-- JS  -->
</body>

</html>