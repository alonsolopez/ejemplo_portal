<?php
//verificar que Admin o Almacenista haya hecho sesión
session_start();
if (isset($_SESSION['usuarioLogeado'])) {
    //si hay sesión se obtienen los datos del tipo de Usuario
    $tipoUs = $_SESSION['usuarioLogeado']['tipo_usuario'];
} else { //si no hay sesion, que se vaya a LOGIN
    header('location:../login.php');
}

//se carga codigo de Producto para CRUD
require_once '../../../middleware/productos/Producto.php';
//También se incluyen CATEGORIA y MARCA, para los SELECT del form del insert
require_once '../../../middleware/productos/Marca.php';
require_once '../../../middleware/productos/Categoria.php';
//se crea instancia de Producto VACIO
$producto = new Producto('', '', '', '', '', '', '', '');
//se instancia Marca y Categoria, para consultar sus registros, 
// y general los SELECT del form
$marca = new Marca();
$categoria = new Categoria();

//consultas de marca y categoria, se realizan SIEMPRE!


//codigo para modifica
if (isset($_POST['btnProducto'])) {
    //obtenemos los datos
    //validando cada input
    if (isset($_POST['txtId']))           $txtID = $_POST['txtId'];
    if (isset($_POST['txtNombre']))       $txtNombre = $_POST['txtNombre'];
    if (isset($_POST['txtDescripcion']))  $txtDescripcion = $_POST['txtDescripcion'];
    if (isset($_POST['txtPrecio']))       $txtPrecio = $_POST['txtPrecio'];
    if (isset($_POST['txtCodBarras']))    $txtCodBarras = $_POST['txtCodBarras'];

    if (isset($_POST['selectMarca']))          $selectMarca = $_POST['selectMarca'];
    if (isset($_POST['selectCategoria']))      $selectCategoria = $_POST['selectCategoria'];
    if (isset($_POST['selectUnidadDeMedida'])) $selectUnidadDeMedida = $_POST['selectUnidadDeMedida'];
    if (isset($_POST['checkEsPerecedero']))    $checkEsPerecedero = $_POST['checkEsPerecedero'];
    else $checkEsPerecedero = 0;


    //FALTAN
    if (isset($_FILES['imagen']))    $imagen = $_FILES['imagen'];


    $target_dir = "../../../../public/images/productos/";
    $target_file_mostrar = "images/productos/" . basename($_FILES["imagen"]["name"]);
    $target_file = $target_dir . basename($_FILES["imagen"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    if (isset($_POST["imagen"])) {
        $check = getimagesize($_FILES["imagen"]["tmp_name"]);
        if ($check !== false) {
            echo "Archivo de image tipo - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "ERROR, El archivo NO ES IMAGEN.";
            $uploadOk = 0;
        }
    }
    // verificar si el archivo ya existe
    if (file_exists($target_file)) {
        echo "Archivo de IMAGEN ya existente.";
        die("Error, archivo de imagen ya existe");
        $uploadOk = 0;
    }

    // Verificar tamaño de archivo max 5mb
    if ($_FILES["imagen"]["size"] > 500000) {
        echo "Error, tamaño de la imagen muy grande.";
        die("Error, archivo de imagen muy grande");
        $uploadOk = 0;
    }

    // PErtmitir los formatos de IMAGEN, todos menos tiff y bmp, ico
    if (
        $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif"
    ) {
        echo "Error, solamente se aceptan los FORMATOS: JPG, JPEG, PNG & GIF.";
        die("Error, archivo de imagen en Formato no válido");
        $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Error, archivo de IMAGEN NO SUBIDO.";
        die("Error, bandera no válida");
        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["imagen"]["tmp_name"], $target_file)) {
            echo "El archivo " . basename($_FILES["imagen"]["name"]) . " ha sido subido.";
        } else {
            echo "Error en el último paso de UPLOAD.";
            die("Error, no subido en mover archiv a servidor");
        }
    }



    //pasamos los datos por Setters al obj producto
    $producto->setNombre($txtNombre);
    $producto->setDescripcion($txtDescripcion);
    $producto->setPrecio($txtPrecio);
    $producto->setCodBarras($txtCodBarras);
    $producto->setMarca($selectMarca);
    $producto->setCategoria($selectCategoria);
    $producto->setUnidadDeMedida($selectUnidadDeMedida);
    $producto->setEsPerecedero($checkEsPerecedero);
    $producto->setImagenes($target_file_mostrar);

    //ejecutamos el metodo insertar del obj producto
    //Y me devuelve el ID de ese producto registrado!!!!
    $res = $producto->insertar();
    //url imagen!! ../../../../public/<?php echo $prod['imagen_producto']; 
    // echo "res=" . $res;
    // die('depurando despues de update');
    //verificar que si se haya actualizado
    if ($res) {
        echo "<script>
    alert('Si se INSERTÓ el registro. RES=" . $res . ");
    </script>";
        header('location:home_crud.php');
    } else echo "Error, no se actualizaron productos";
}


// //verificar si llegó producto_id por POST
// if (isset($_GET['producto_id']) && $_GET['producto_id'] > 0) {
//     // var_dump($producto);

//     $prod = $producto->consultaPorID($_GET['producto_id'])[0];
//     //mostrar todos los datos

// } else {
//     header('location:./home_crud.php');
// }



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portal empresarial - PDV UTH v1</title>
    <!-- CSS -->
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
    <link href="../../css/main.css" rel="stylesheet">
    <link href="../../css/responsive.css" rel="stylesheet">
    <!-- CSS -->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
</head>

<body>
    <div class="container">
        <?php include "../includes/header.php"; ?>

        <div class="container">
            <div class="row">

                <div class="col-sm-2" id="menu-left">
                    <h4>Menu left</h4>
                    <ul>
                        <li><a href="#1">opcion1</a></li>
                        <li><a href="#2">opcion2</a></li>
                        <li><a href="#3">opcion3</a></li>
                        <li><a href="#4">opcion4</a></li>
                        <li><a href="#5">opcion5</a></li>
                        <li><a href="#6">opcion6</a></li>
                    </ul>
                </div>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Nuevo Producto</h2>
                            <hr>
                            <!--  -->
                            <form action="" method="POST" enctype="multipart/form-data">
                                <!-- datos -->

                                <div class="form-group">
                                    <label for="txtNombre">Nombre</label>
                                    <input type="text" class="form-control" name="txtNombre" id="txtNombre"
                                        aria-describedby="nombreHelp">
                                    <small id="nombreHelp" class="form-text text-muted">El nombre del producto
                                        registrado.</small>
                                </div>
                                <div class="form-group">
                                    <label for="txtDescripcion">Descripcion</label>
                                    <textarea class="form-control" id="txtDescripcion" name="txtDescripcion"
                                        aria-describedby="codBarrasHElp"></textarea>
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email
                                        with
                                        anyone else.</small>
                                </div>
                                <div class="form-group">
                                    <label for="txtPrecio">Precio</label>
                                    <input type="text" class="form-control" id="txtPrecio" name="txtPrecio"
                                        aria-describedby="precioHelp">
                                    <small id="precioHelp" class="form-text text-muted">Precio del producto
                                        registrado
                                        .</small>
                                </div>
                                <div class="form-group">
                                    <label for="txtCategoria">Código de Barras</label>
                                    <input type="text" class="form-control" name="txtCodBarras" id="txtCodBarras"
                                        aria-describedby="emailHelp">
                                    <small id="codBarrasHElp" class="form-text text-muted">El código de Barras de
                                        producto.</small>
                                </div>
                                <div class="form-group">
                                    <label for="selectMarca">Marca</label>
                                    <select class="form-control" name="selectMarca" id="selectMarca"
                                        aria-describedby="marcaHElp">
                                        <option value="0" selected>Selecciona ...</option>
                                        <?php
                                        //rellenamos los options con las marcas
                                        foreach ($marca->consultarTodas() as $key => $value) {
                                            //generamos los options
                                        ?>
                                        <option value="<?php echo $value["id"]; ?>"
                                            title="<?php echo $value["descripcion"]; ?>"><?php echo $value["nombre"]; ?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    <small id="marcaHElp" class="form-text text-muted">La Marca de
                                        producto.</small>
                                </div>

                                <div class="form-group">
                                    <label for="selectCategoria">Categoria</label>
                                    <select class="form-control" name="selectCategoria" id="selectCategoria"
                                        aria-describedby="categoriaHElp">
                                        <option value="0" selected>Selecciona ...</option>
                                        <?php echo $categoria->consultarOptionsParaSelect(); ?>
                                    </select>
                                    <small id="categoriaHElp" class="form-text text-muted">La categoria de
                                        producto.</small>
                                </div>

                                <div class="form-group">
                                    <label for="selectUnidadDeMedida">Unidad de Medida</label>
                                    <select class="form-control" name="selectUnidadDeMedida" id="selectUnidadDeMedida"
                                        aria-describedby="categoriaHElp">
                                        <option value="0" selected>Selecciona ...</option>
                                        <option value="1">UNIDAD</option>
                                        <option value="2">LITRO</option>
                                        <option value="3">KILO</option>
                                        <option value="4">CAJA</option>
                                        <option value="5">PAQUETE</option>
                                    </select>
                                    <small id="categoriaHElp" class="form-text text-muted">La categoria de
                                        producto.</small>
                                </div>

                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">¿Perecedero?</label>
                                </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="imagen">Imagen del Producto</label>
                                <input type="file" name="imagen" id="imagen" aria-describedby="imagenHelp">
                                <small id="imagenHelp" class="form-text text-muted">La imagen del
                                    producto.</small>
                            </div>
                            <!-- imagen -->
                            <div class="card" style="width: 18rem;">
                                <label for="imagen">Imagen</label>

                                <img src="" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <p class="card-text">La Imagen de producto..</p>
                                </div>
                            </div>



                        </div>


                    </div>
                    <hr>
                    <!-- button -->
                    <button type="submit" class="btn btn-success" name="btnProducto">Guardar</button>
                    </form>

                </div>
            </div>
        </div>
        <?php include '../includes/footer.php'; ?>
    </div>
    <!-- JS  -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>

    <!-- JS  -->
</body>

</html>