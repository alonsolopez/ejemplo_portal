<?php
//verificar que Admin o Almacenista haya hecho sesión
session_start();
if (isset($_SESSION['usuarioLogeado'])) {
    //si hay sesión se obtienen los datos del tipo de Usuario
    $tipoUs = $_SESSION['usuarioLogeado']['tipo_usuario'];
} else { //si no hay sesion, que se vaya a LOGIN
    header('location:../login.php');
}

//se carga codigo de Producto para CRUD
require_once '../../../middleware/productos/Producto.php';
//se crea instancia de Producto VACIO
$producto = new Producto('', '', '', '', '', '', '', '');

//verificar si llegó producto_id por POST
if (isset($_GET['producto_id']) && $_GET['producto_id'] > 0) {
    // var_dump($producto);

    $producto->eliminar($_GET['producto_id']);
    header('location:./home_crud.php');
}

header('location:./home_crud.php');