<?php
//verificar que Admin o Almacenista haya hecho sesión
session_start();
if (isset($_SESSION['usuarioLogeado'])) {
    //si hay sesión se obtienen los datos del tipo de Usuario
    $tipoUs = $_SESSION['usuarioLogeado']['tipo_usuario'];
} else { //si no hay sesion, que se vaya a LOGIN
    header('location:login.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portal empresarial - PDV UTH v1</title>
    <!-- CSS -->
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!-- CSS -->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
</head>

<body>
    <div class="container">

        <?php include "./pages/includes/header.php"; ?>

        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-lg-5 bg-verde-sarra" id="menu-left">
                    <h4>Menu left</h4>
                    <ul>
                        <li><a href="#1">opcion1</a></li>
                        <li><a href="#2">opcion2</a></li>
                        <li><a href="#3">opcion3</a></li>
                        <li><a href="#4">opcion4</a></li>
                        <li><a href="#5">opcion5</a></li>
                        <li><a href="#6">opcion6</a></li>
                    </ul>
                </div>
                <div class="col-sm-9 col-lg-7">
                    <main>
                        <h2>MAIN</h2>
                    </main>
                </div>
            </div>
        </div>
        <?php include './pages/includes/footer.php'; ?>
    </div>
    <!-- JS  -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>

    <!-- JS  -->
</body>

</html>