<?php
session_start(); //se va a checar $_SESSION, se necesita 'session_start()'
//verificar que el cliente se ha logeado
if (isset($_SESSION['clienteLogeado']) && $_SESSION['clienteLogeado'] != null) {
    //si hay logeado, se muestra
    // header('location:./public/cart.php');
    echo 'YA ESTAS LOGEADO';
} else {

    //required CLIENTE
    require_once "../app/middleware/clientes/Cliente.php";
    //si se ingreso datos en el FORM
    $mail = "";
    $pass = "";
    if (isset($_POST['btnLogin'])) {
        //giardamos los datos
        if (isset($_POST['correoLogin']))
            $mail = $_POST['correoLogin'];
        if (isset($_POST['passwordLogin']))
            $pass = $_POST['passwordLogin'];
        //creamos la instancia
        $cliente = new Cliente('', '', '', '', '', '', '', '', '');
        $logeado = 'nada';
        //ejecutamos el login
        if (($logeado = $cliente->login($mail, $pass)) && $logeado != null) {
            // var_dump($logeado);
            // die();
            //crear las vars de sesión
            $_SESSION['clienteLogeado'] = $logeado[0];
            var_dump($_SESSION['clienteLogeado']);
            //die();
            //abrimos HOME
            header("location:cuenta.php");
        } else {
            echo "Error al ingresar, verifique sus datos e inténtelo de nuevo";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login PDV UTH v1</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->

<body>
    <!-- header --><?php require_once 'includes/header.php'; ?>

    <section id="form">
        <!--form-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="login-form">
                        <!--login form-->
                        <h2>Ingresa a tu Cuenta</h2>
                        <form action="" method="POST">
                            <input type="email" placeholder="Correo" name="correoLogin" />
                            <input type="password" placeholder="Contraseña" name="passwordLogin" />
                            <span>
                                <input type="checkbox" class="checkbox">
                                Mantenme contectado
                            </span>
                            <button type="submit" name="btnLogin" class="btn btn-default">Login</button>
                        </form>
                    </div>
                    <!--/login form-->
                </div>
                <div class="col-sm-1">
                    <h2 class="or">Ó</h2>
                </div>
                <div class="col-sm-4">
                    <div class="signup-form">
                        <!--sign up form-->
                        <h2>¡Registro de nuevo cliente!</h2>
                        <form action="" method="POST">
                            <input type="text" placeholder="Nombre" name="nombreRegistro" />
                            <input type="email" placeholder="Correo" name="correoRegistro" />
                            <input type="text" placeholder="Contraseña" name="passwordRegistro" />
                            <button type="submit" class="btn btn-default">Registrarse</button>
                        </form>
                    </div>
                    <!--/sign up form-->
                </div>
            </div>
        </div>
    </section>
    <!--/form-->


    <!-- footer -->
    <?php require 'includes/footer.php' ?>


    <script src="js/jquery.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>

</html>