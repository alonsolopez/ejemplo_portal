<?php
session_start(); //se va a checar $_SESSION, se necesita 'session_start()'
//verificar que el cliente se ha logeado
if (isset($_SESSION['clienteLogeado']) && $_SESSION['clienteLogeado'] != null) {
    //si hay logeado, se muestra
    // header('location:./public/cart.php');
} else {
    //si no hay cliente logeado, se abre el login
    header('location:./login.php');
}

// proceso de los datos del REVIEW
if (isset($_POST['btnSubmit'])) {

    //procesamos los demás--------------
    //el calificar se toma el VALUE que se haya seleccionado por último
    $calificacion = isset($_POST['calificar']) ? $_POST['calificar'] : 0;
    //lo mismo con opinion
    $opinion =
        isset($_POST['opinion']) ? $_POST['opinion'] : "Error, no se enviaron DATOS";
    //sacamos TODOS el objeto del CLIENTE LOGEADO en la sesión
    $cliente = $_SESSION['clienteLogeado'];
    //////////////////////////////////////////////////////////
    //se incluye la clase OpinionReview y se instancia
    require_once dirname(dirname(__FILE__)) . '/app/middleware/clientes/OpinionReview.php';
    // require_once dirname(dirname(dirname(__FILE__))) . '/app/middleware/bd/CriterioDeBusqueda.php';
    $opinionReview = new OpinionReview(0, null, 0, '');
    //tomamos los datos para registro
    $res = $opinionReview->registraOpinionReview($cliente, $calificacion, $opinion);
    //validamos el RES
    if ($res == true) {
        echo "<script>alert('Tu opinión ha sido registrada :)');</script>";
    } else {
        echo "<script>alert('Error...inténtalo de nuevo');</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Registro de Review | E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <style>
    .star_content {
        margin-bottom: 5px;
        float: left;
        width: 100%;
    }

    .star {
        overflow: hidden;
        float: left;
        margin: 0 1px 0 0;
        width: 16px;
        height: 18px;
        cursor: pointer;
    }

    .star a {
        display: block;
        width: 100%;
        background-position: 0 0;
    }

    .star {
        position: relative;
        top: -1px;
        float: left;
        width: 14px;
        overflow: hidden;
        cursor: pointer;
        font-size: 14px;
        font-weight: normal;
    }

    .star a {
        display: block;
        position: absolute;
        text-indent: -5000px;
    }

    div.star:after {
        content: "\f006";
        font-family: "FontAwesome";
        display: inline-block;
        color: #777676;
    }

    div.star.star_on {
        display: block;
    }

    div.star.star_on:after {
        content: "\f005";
        font-family: "FontAwesome";
        display: inline-block;
        color: #ef8743;
    }

    div.star.star_hover:after {
        content: "\f005";
        font-family: "FontAwesome";
        display: inline-block;
        color: #ef8743;
    }
    </style>
</head>
<!--/head-->

<body>
    <!-- header --><?php require_once 'includes/header.php'; ?>
    <div class="container">
        <div class="bg">

            <div class="row">
                <div class="col-sm-8">
                    <div class="contact-form">
                        <h2 class="title text-center">¡ Califica tu Experiencia PDV UTH !</h2>
                        <!-- <div class="status alert alert-success" style="display: none"></div> -->
                        <!-- EL FORM no define action, por lo que los datos llegan aquí mismo -->
                        <form id="opinionReview-form" class="contact-form row" name="contact-form" method="post">


                            <div class="form-group col-md-12">
                                <h4>Selecciona con tu mouse tu calificación </h4>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="star_content">
                                    <input name="calificar" value="1" type="radio" class="star" />
                                    <input name="calificar" value="2" type="radio" class="star" />
                                    <input name="calificar" value="3" type="radio" class="star" />
                                    <input name="calificar" value="4" type="radio" class="star" checked="checked" />
                                    <input name="calificar" value="5" type="radio" class="star" />
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <textarea name="opinion" class="form-control" required="required"
                                    placeholder="Registra tu opinión sobre PDV UTH"></textarea>
                            </div>

                            <div class="form-group col-md-12">
                                <input type="submit" name="btnSubmit" class="btn btn-primary pull-right"
                                    value="Enviar tu Opinión">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="contact-info">
                        <h2 class="title text-center">Información de Contacto</h2>
                        <address>
                            <p>PDV UTH v1.</p>
                            <p>Blvd. de los Seris Final, s/n.</p>
                            <p>Hermorancho, Sonora</p>
                            <p>Cel: +52 6622514100</p>
                            <p>Correo: pdv_uth@uthermosillo.edu.mx</p>
                        </address>
                        <div class="social-networks">
                            <h2 class="title text-center">Redes Sociales</h2>
                            <ul>
                                <li>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-youtube"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- footer -->
    <?php require 'includes/footer.php' ?>



    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
    <script src="js/jquery.rating.pack.js"></script>
    <script>
    $(document).ready(function() {
        $('input.star').rating();
    });
    </script>
</body>

</html>