<?php
session_start(); //se va a checar $_SESSION, se necesita 'session_start()'
//verificar que el cliente se ha logeado
if (isset($_SESSION['clienteLogeado']) && $_SESSION['clienteLogeado'] != null) {
    //si hay logeado, se muestra
    // header('location:./public/cart.php');
} else {
    //si no hay cliente logeado, se abre el login
    header('location:./login.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>PDV UTH v1</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->

<body>
    <!-- header --><?php require_once 'includes/header.php'; ?>


    <!--/slider--><?php require_once 'includes/slider.php'; ?>

    <section>
        <div class="container" id="myDiv">
            <div class="row">
                <div class="col-sm-3">

                    <h1>Tu cuenta</h1>
                    <p><b>Nombre:</b><?php echo $_SESSION['clienteLogeado']['nombre']; ?></p><br>
                    <p><b>Correo:</b><?php echo $_SESSION['clienteLogeado']['correo']; ?></p><br>
                    <hr>
                    <p>Tus Pedidos:</p>
                    <hr>

                </div>
            </div>
        </div>
    </section>

    <!-- footer -->
    <?php require 'includes/footer.php' ?>



    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>

</body>

</html>