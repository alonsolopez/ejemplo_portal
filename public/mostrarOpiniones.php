<?php
//no necesitamos que esté logeado el cliente para ver las opiniones
// session_start(); //se va a checar $_SESSION, se necesita 'session_start()'
// //verificar que el cliente se ha logeado
// if (isset($_SESSION['clienteLogeado']) && $_SESSION['clienteLogeado'] != null) {
//     //si hay logeado, se muestra
//     // header('location:./public/cart.php');
// } else {
//     //si no hay cliente logeado, se abre el login
//     header('location:./login.php');
// }

// consulta de los datos del REVIEWs

//////////////////////////////////////////////////////////
//se incluye la clase OpinionReview y se instancia
require_once dirname(dirname(__FILE__)) . '/app/middleware/clientes/OpinionReview.php';
require_once dirname(dirname(__FILE__)) . '/app/middleware/clientes/Cliente.php';
require_once dirname(dirname(__FILE__)) . '/app/middleware/bd/CriterioDeBusqueda.php';
$opinionReview = new OpinionReview(0, null, 0, '');
//criterios a consultar (TODOS!!! se hace WHERE 1=1)
$criterios = [
    "0" => new CriterioDeBusqueda('1', CriterioDeBusqueda::OP_IGUAL, 1, false, CriterioDeBusqueda::OP_LOGICO_NONE),
];
//consultamos todas las opiniones
$opiniones = $opinionReview->consultarTodos();
//validamos el que SI haya registros realizados
if ($opiniones == null) {
    echo "<script>alert('Aún no se han registrado opiniones');</script>";
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Consulta de Registros de Reviews | E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <style>
    .checked {
        color: orange;
    }
    </style>
</head>
<!--/head-->

<body>
    <!-- header --><?php require_once 'includes/header.php'; ?>
    <div class="container">
        <div class="bg">

            <div class="row">
                <div class="col-sm-8">
                    <div class="contact-form">
                        <?php
                        foreach ($opiniones as $key => $value) {

                            # mapear cliente segun su ID
                            $cliente = new Cliente($value['cliente_id'], '', '', '', '', '', '', '', '');
                            //se utiliza el metodo de MAPEO por ID
                            $cliente->mapearPorId($value['cliente_id']);
                        ?>
                        <div class="card">
                            <h5><?php echo 'Cliente = ' . $cliente->getNombre(); ?></h5>
                            <div class="form-group col-md-6">
                                <div class="star_content">
                                    <!-- ejemplo base html/css de https://www.w3schools.com/howto/howto_css_star_rating.asp -->
                                    <span
                                        class="fa fa-star <?php echo $value['calificacion'] >= 1 ? 'checked' : ' '; ?>"></span>
                                    <span
                                        class="fa fa-star <?php echo $value['calificacion'] >= 2 ? 'checked' : ' '; ?>"></span>
                                    <span
                                        class="fa fa-star <?php echo $value['calificacion'] >= 3 ? 'checked' : ' '; ?>"></span>
                                    <span
                                        class="fa fa-star <?php echo $value['calificacion'] >= 4 ? 'checked' : ' '; ?> "></span>
                                    <span
                                        class="fa fa-star <?php echo $value['calificacion'] >= 5 ? 'checked' : ' '; ?> "></span>

                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <p><?php echo $value['opinion']; ?></p>
                            </div>
                        </div>
                        <?php
                        } //foreach opiniones 
                        ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="contact-info">
                        <h2 class="title text-center">Información de Contacto</h2>
                        <address>
                            <p>PDV UTH v1.</p>
                            <p>Blvd. de los Seris Final, s/n.</p>
                            <p>Hermorancho, Sonora</p>
                            <p>Cel: +52 6622514100</p>
                            <p>Correo: pdv_uth@uthermosillo.edu.mx</p>
                        </address>
                        <div class="social-networks">
                            <h2 class="title text-center">Redes Sociales</h2>
                            <ul>
                                <li>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-youtube"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- footer -->
    <?php require 'includes/footer.php' ?>



    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
    <!-- <script src="js/jquery.rating.pack.js"></script>
    <script>
    $(document).ready(function() {
        $('input.star').rating();
    });
    </script> -->
</body>

</html>