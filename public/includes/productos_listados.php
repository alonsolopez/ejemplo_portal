<?php
//crear instancia de Productos
require_once dirname(dirname(dirname(__FILE__))) . '/app/middleware/productos/Producto.php';
// require_once dirname(dirname(dirname(__FILE__))) . '/app/middleware/bd/CriterioDeBusqueda.php';
$producto = new Producto('', '', '', '', '', '', '', '');

//criterios de selección--------------------
//MARCAS--------
$marca = null;
if (isset($_GET['marca_id'])) $marca = $_GET['marca_id'];
//CATEFORIAS--------
$categoria = null;
if (isset($_GET['categoria_id'])) $categoria = $_GET['categoria_id'];

if (isset($marca))
    $criterios = [
        "0" => isset($marca) ? new CriterioDeBusqueda('marca_id', CriterioDeBusqueda::OP_IGUAL, $marca, false, CriterioDeBusqueda::OP_LOGICO_NONE) : new CriterioDeBusqueda('categoria_id', CriterioDeBusqueda::OP_IGUAL, $categoria, false, CriterioDeBusqueda::OP_LOGICO_NONE),
    ];
else
    $criterios = [
        "0" => new CriterioDeBusqueda('marca_id', CriterioDeBusqueda::OP_DIFERENTE, 3, false, CriterioDeBusqueda::OP_LOGICO_NONE),
    ];

foreach ($producto->consultarPorCriterios($criterios) as $key => $value) {
    #todos los productos
    // var_dump($value);
?>
    <div class="col-sm-4">
        <div class="product-image-wrapper">
            <div class="single-products">
                <div class="productinfo text-center">
                    <img src="<?php echo $value['imagen_producto']; ?>" alt="" />
                    <h2>$<?php echo $value['precio']; ?></h2>
                    <p><?php echo $value['nombre']; ?></p>
                    <a href="cart.php?producto_id=<?php echo $value['id']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Agrega al Carrito</a>
                </div>
                <div class="product-overlay">
                    <div class="overlay-content">
                        <h2>$<?php echo $value['precio']; ?></h2>
                        <p><?php echo $value['nombre']; ?></p>
                        <a href="cart.php?producto_id=<?php echo $value['id']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Agrega al
                            Carrito</a>
                    </div>
                </div>
            </div>
            <div class="choose">
                <ul class="nav nav-pills nav-justified">
                    <li><a href="#"><i class="fa fa-plus-square"></i>Agrega a Lista de Deseos</a></li>
                    <!-- <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li> -->
                </ul>
            </div>
        </div>
    </div>
<?php
}
?>


<!-- resto de productos en index.php -->
<!-- <div class="col-sm-4">
    <div class="product-image-wrapper">
        <div class="single-products">
            <div class="productinfo text-center">
                <img src="images/home/product4.jpg" alt="" />
                <h2>$56</h2>
                <p>Easy Polo Black Edition</p>
                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
            </div>
            <div class="product-overlay">
                <div class="overlay-content">
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                </div>
            </div>
            <img src="images/home/new.png" class="new" alt="" />
        </div>
        <div class="choose">
            <ul class="nav nav-pills nav-justified">
                <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="product-image-wrapper">
        <div class="single-products">
            <div class="productinfo text-center">
                <img src="images/home/product5.jpg" alt="" />
                <h2>$56</h2>
                <p>Easy Polo Black Edition</p>
                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
            </div>
            <div class="product-overlay">
                <div class="overlay-content">
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                </div>
            </div>
            <img src="images/home/sale.png" class="new" alt="" />
        </div>
        <div class="choose">
            <ul class="nav nav-pills nav-justified">
                <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="product-image-wrapper">
        <div class="single-products">
            <div class="productinfo text-center">
                <img src="images/home/product6.jpg" alt="" />
                <h2>$56</h2>
                <p>Easy Polo Black Edition</p>
                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
            </div>
            <div class="product-overlay">
                <div class="overlay-content">
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                </div>
            </div>
        </div>
        <div class="choose">
            <ul class="nav nav-pills nav-justified">
                <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
            </ul>
        </div>
    </div>
</div> -->